#include <string.h>
#include "strings.h"

char get_max_arr_len_P(PGM_P const *arr, char arr_len)
{
    char buf[127], i, max_len=0, len;
    buf[126]=0;
    for(i=0;i<arr_len;i++) {
        strncpy_P(buf, (PGM_P)pgm_read_word(&(arr[(unsigned)i])), 126);
        len = strlen(buf);
        if(len>max_len)
            max_len = len;
    }
    return max_len;
}

char get_max_arr_len(char* const *arr, char arr_len)
{
    char *ptr, i, max_len=0, len;
    for(i=0;i<arr_len;i++) {
        ptr=arr[i];
        len = strlen(ptr);
        if(len>max_len)
            max_len = len;
    }
    return max_len;
}

char *find_quoted_str(char *buf) {
    char *buf_end;

    while(*buf!='"' && *buf)buf++;
    if(!buf)return NULL;
    buf++;
    buf_end=buf;
    while(*buf_end!='"' && *buf_end)buf_end++;
    if(!buf_end)return NULL;
    *buf_end=0;
    
    return buf;
}
