#ifndef _UTIL_H_
#define _UTIL_H_

#define _BSD_SOURCE
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "eeprom.h"

#define PROGMEM
#define EEMEM

#define read_rom(a) *(a)
#define memcpy_P(src,dst,size) memcpy(src,dst,size)
#define strcpy_P(src,dst) strcpy(src,dst)
#define strncpy_P(src,dst,size) strncpy(src,dst,size)
#define strcmp_P(src,dst) strcmp(src,dst)
#define pgm_read_byte(addr) (*(const char *)addr)
#define _BV(bit) (1 << (bit))

inline const long pgm_read_word(const void *addr) 
{
    const long *ptr=addr;
    return *ptr;
}

#define PGM_P const char *

#define debug_print(a, ...) printf(a,## __VA_ARGS__)
#define debug_puts(a) printf(a)

#endif /*_UTIL_H_*/
