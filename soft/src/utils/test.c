#include "strconv.h"
#include <stdio.h>

void test(char *str)
{
    printf("%s: %d\n",str, dec2short(str));
    
}

int main(int argc, char *argv[]) 
{
    test("12345");
    test("-12345");
    test("0");
    test("-0");
    test("");
    return 0;
}
