#ifndef _UTILS_UTILS_H_
#define _UTILS_UTILS_H_

short swap_short(short val);
unsigned short swap_ushort(unsigned short val);

#endif /*_UTILS_UTILS_H_*/
