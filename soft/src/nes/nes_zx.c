#include <string.h>
#include <timer/timer.h>
#include <platform/utils.h>
#include <platform/zx_keyb.h>
#include <console/console.h>
#include <nes/nes_controller.h>
#include "nes_zx.h"

static struct timer t;
static char timer_id;
static struct timer t_autofire;
static char timer_id_autofire;
static struct nes_controllers data, prev_data;
static union zx_joy prev_joy[3];
static char autofire_mask;
static struct nes_controllers data;

#define NES_ZX_DEBUG

#ifdef NES_ZX_DEBUG
PROGMEM static const char STR_CNTR[] = "Conroller[";
#endif

static void nes_zx_timer_autofire_callback(unsigned char timer_id, const void *unused)
{
    autofire_mask^=1;
}

static union zx_joy *nes_zx_joy_transform(union nes_controller *c, 
                                          union zx_joy *j, 
                                          union zx_joy *prev_joy,
                                          union nes_controller *prev_data,
                                          char *is_pressed,
                                          char idx)
{
    unsigned char is_fire=~(c->bits.a & c->bits.b);
    unsigned char is_auto_fire=~c->bits.start;

    j->data=prev_joy->data;

    if(c->data != prev_data->data) {
#ifdef NES_ZX_DEBUG
        console_puts_P(STR_CNTR);
        console_putd(idx);
        console_putc(']');
        console_putc('=');
        console_putmem(&c->data, 2);
        console_next_line();
#endif
        if(c->data != 0xffff) 
            *is_pressed=1;
        prev_data->data=c->data;
        j->bits.left=~c->bits.left;
        j->bits.right=~c->bits.right;
        j->bits.up=~c->bits.up;
        j->bits.down=~c->bits.down;
        j->bits.fire=is_fire;
    }

    if(!is_fire && is_auto_fire) {
      j->bits.fire=autofire_mask;
      if(!*is_pressed)
          *is_pressed=autofire_mask;
    }

    if(j->data != prev_joy->data) {
        prev_joy->data=j->data;
        return j;
    }
    return NULL;
}
 
static void nes_zx_timer_callback(unsigned char timer_id, const void *unused)
{
    union zx_joy j, *j_ret;
    char is_pressed=0;

    nes_controller_read(&data);

    if(data.controller[0].present)
        j_ret=nes_zx_joy_transform(&data.controller[0].c, &j, &prev_joy[0], &prev_data.controller[0].c, &is_pressed, 0);
    else
        j_ret=NULL;
    if(j_ret!=NULL) {
        zx_keyb_set_kepston(j_ret);
    }

    if(data.controller[1].present)
        j_ret=nes_zx_joy_transform(&data.controller[1].c, &j, &prev_joy[1], &prev_data.controller[1].c, &is_pressed, 1);
    else
        j_ret=NULL;
    if(j_ret!=NULL) {
        zx_keyb_set_sinclair1(j_ret);
    }

    if(data.controller[2].present)
        j_ret=nes_zx_joy_transform(&data.controller[2].c, &j, &prev_joy[2], &prev_data.controller[2].c, &is_pressed, 2);
    else
        j_ret=NULL;
    if(j_ret!=NULL) {
        zx_keyb_set_sinclair2(j_ret);
    }

    zx_keyb_led(is_pressed);
}

void nes_zx_start()
{
    autofire_mask=0;
    prev_joy[0].data=prev_joy[1].data=prev_joy[2].data=0;
    memset(&prev_data, 0xff, sizeof(prev_data));

    nes_controller_init(&data);
    zx_keyb_init();

    t.period=50;
    t.callback=nes_zx_timer_callback;
    timer_id=timer_setup(&t);

    t_autofire.period=150;
    t_autofire.callback=nes_zx_timer_autofire_callback;
    timer_id_autofire=timer_setup(&t_autofire);
}

void nes_zx_done()
{
    timer_free(timer_id);
    timer_free(timer_id_autofire);
}
