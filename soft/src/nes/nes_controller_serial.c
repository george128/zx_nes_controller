#include <string.h>
#include <platform/nes_controller_serial.h>

#include "nes_controller.h"

static void nes_controller_transform(union nes_controller_serial *from, union nes_controller *to)
{
    to->bits.a = from->bits.a;
    to->bits.b = from->bits.b;
    to->bits.sel = from->bits.sel;
    to->bits.start = from->bits.start;
    to->bits.up = from->bits.up;
    to->bits.down = from->bits.down;
    to->bits.left = from->bits.left;
    to->bits.right = from->bits.right;
}

void nes_controller_init()
{
    nes_controller_serial_init();
}

void nes_controller_read(struct nes_controllers *data)
{
    struct nes_controllers_serial c;

    nes_controller_serial_read(&c);
    data->controller[0].present=c.p1;
    nes_controller_transform(&c.c1,&data->controller[0].c);
    data->controller[1].present=c.p2;
    nes_controller_transform(&c.c2,&data->controller[1].c);
    data->controller[2].present=0;
    memset(&data->controller[2].c, 0, sizeof(data->controller[2].c));
}
