#include <string.h>
#include <platform/utils.h>
#include <platform/i2c.h>
#include <console/console.h>
#include <tca9548/tca9548.h>
#include "nes_controller.h"

#undef NES_CONTROLLER_DEBUG

PROGMEM static const char STR_NES_CNTR_ID[]="nes controller id:";
PROGMEM static const char STR_NES_CNTR_WRITE_ERR[]="nes controller write error: ";
PROGMEM static const char STR_NES_CNTR_READ_ERR[]="nes controller read error: ";
PROGMEM static const char STR_FOUND_CONTROLLER[]="Found NES controller: ";

#ifdef NES_CONTROLLER_DEBUG
PROGMEM static const char STR_NES_CNTR_DATA[]="nes controller data:";
#endif

static char nes_controller_init_joy(char no)
{
    char buf[256];
    char ret;

    if(!i2c_switch_set_line(no))
        return 0;

    /*disable encryption*/
    buf[0]=0x55;
    ret=i2c_write(I2C_DEV_NO_NES_CLASSIC,0xf0,1,buf);
    if(ret != 1) {
        console_putd10(no);
        console_putc('1');
        console_put_msg(STR_NES_CNTR_WRITE_ERR, ret);
        return 0;
    }
    _delay_ms(20);
    buf[0]=0;
    ret=i2c_write(I2C_DEV_NO_NES_CLASSIC,0xfb,1,buf);
    if(ret != 1) {
        console_putd10(no);
        console_putc('2');
        console_put_msg(STR_NES_CNTR_WRITE_ERR, ret);
        return 0;
    }
    _delay_ms(20);

    /*read id*/
    i2c_write(I2C_DEV_NO_NES_CLASSIC,0xfa,0,NULL);
    ret=i2c_read(I2C_DEV_NO_NES_CLASSIC,0xff,6,buf);
    if(ret != 6) {
        console_putd10(no);
        console_put_msg(STR_NES_CNTR_READ_ERR, ret);
        return 0;
    }
    console_puts_P(STR_NES_CNTR_ID);
    console_putmem(buf,6);
    console_next_line();
    /* prepare to read keys*/
    i2c_write(I2C_DEV_NO_NES_CLASSIC,0x0,0,NULL);

    return 1;
}

static void nes_controller_read_joy(char no, union nes_controller *data)
{
    char buf[16], ret;

    memset(data,0xff,sizeof(*data));

    if(!i2c_switch_set_line(no))
        return;

    ret=i2c_read(I2C_DEV_NO_NES_CLASSIC,0xff,8,buf);
    if(ret != 8) {
        console_putd10(no);
        console_putc('2');
        console_put_msg(STR_NES_CNTR_READ_ERR, ret);
        return;
    }
#ifdef NES_CONTROLLER_DEBUG
    if((unsigned char)buf[6] != 0xff || (unsigned char)buf[7] != 0xff) {
        console_puts_P(STR_NES_CNTR_DATA);
        console_putmem(buf,8);
        console_putc('\n');
    }
#endif
    memcpy(data, buf+6, 2);
    /* prepare to read keys*/
    i2c_write(I2C_DEV_NO_NES_CLASSIC,0x0,0,NULL);
}

void nes_controller_init(struct nes_controllers *data)
{
    char no;

    i2c_init();
    //i2c_test();
    _delay_ms(100);
    for(no=0;no<3;no++) {
        data->controller[no].present=nes_controller_init_joy(no);
        if(data->controller[no].present) {
            console_put_msg(STR_FOUND_CONTROLLER,no);
        }
    }
}

void nes_controller_read(struct nes_controllers *data)
{
    char no;

    for(no=0;no<3;no++) {
        if(data->controller[no].present) {
            nes_controller_read_joy(no,&data->controller[no].c);
        }
    }
}
