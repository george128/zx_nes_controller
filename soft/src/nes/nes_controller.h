#ifndef _NES_CONTROLLER_H_
#define _NES_CONTROLLER_H_

union nes_controller {
    unsigned short data;
    struct {
        int unused1:2;
        int start:1;
        int unused2:1;
        int sel:1;
        int unused3:1;
        int down:1;
        int right:1;

        int up:1;
        int left:1;
        int unused4:2;
        int a:1;
        int unused5:1;
        int b:1;
        int unused6:1;
    } bits;
};

struct nes_controllers {
    struct {
        char present;
        union nes_controller c;
    } controller[3];
};

void nes_controller_init();
void nes_controller_read(struct nes_controllers *data);

#endif /*_NES_CONTROLLER_H_*/
