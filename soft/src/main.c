#include <timer/timer.h>
#include <console/console.h>
#include <platform/utils.h>
#include <platform/systime.h>
#include <platform/reset_controller.h>
#include <nes/nes_zx.h>

PROGMEM static const char STR_STARTING[]="Starting...\n";
PROGMEM static const char STR_MAIN_LOOP[]="Start main loop\n";

/*void main(void) __attribute__ ((naked));*/

int main(void)
{
    console_init();
    reset_controller_init();
    console_puts_P(STR_STARTING);
    systime_init();
    nes_zx_start();
    reset_controller_start();
    console_puts_P(STR_MAIN_LOOP);
    for(;;) {
        timer_task();
    }
    nes_zx_done();
    return 0;
}
