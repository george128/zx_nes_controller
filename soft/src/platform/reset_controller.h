#ifndef _RESET_CNTROLLER_H_
#define _RESET_CNTROLLER_H_

void reset_controller_init();
void reset_controller_start();
void reset_controller_do_reset();
#endif /*_RESET_CNTROLLER_H_*/
