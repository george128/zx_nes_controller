#include <avr/io.h>
#include <avr/interrupt.h>
#include <platform/utils.h>
#include <console/console.h>
#include "zx_keyb.h"

#define KEMP_PORT PORTB
#define KEMP_DIR DDRB

#define KEMP_LEFT PORTB0
#define KEMP_RIGHT PORTB1
#define KEMP_UP PORTB2
#define KEMP_DOWN PORTB3
#define KEMP_FIRE PORTB4

#define KEMP_ALL (_BV(KEMP_LEFT)|_BV(KEMP_RIGHT)|_BV(KEMP_UP)|_BV(KEMP_DOWN)|_BV(KEMP_FIRE))

#ifdef CONTROLLER_I2C

#define COL_PORT1 PORTC
#define COL_DIR1 DDRC
#define COL_PIN1 PINC
#define COL_PORT2 PORTD
#define COL_DIR2 DDRD
#define COL_PIN2 PIND

#define COL0 PINC0
#define COL1 PINC1
#define COL2 PINC2
#define COL3 PINC3
#define COL4 PIND4

#define COL0INT PCINT8
#define COL1INT PCINT9
#define COL2INT PCINT10
#define COL3INT PCINT11
#define COL4INT PCINT20

#define COL_ALL1 (_BV(COL0)|_BV(COL1)|_BV(COL2)|_BV(COL3))
#define COL_ALL2 _BV(COL4)

#define ROW_PORT PORTD
#define ROW_DIR DDRD
#define ROW_IN PIND

#define ROW3 PORTD2
#define ROW4 PORTD3

#define SYNCL1_LEFT COL0
#define SYNCL1_RIGHT COL1
#define SYNCL1_UP COL2
#define SYNCL1_DOWN COL3
#define SYNCL1_FIRE COL4

#define SYNCL2_LEFT COL4
#define SYNCL2_RIGHT COL3
#define SYNCL2_UP COL2
#define SYNCL2_DOWN COL1
#define SYNCL2_FIRE COL0

#endif /*CONTROLLER_I2C*/

#ifdef CONTROLLER_SERIAL

#define SYNCL1_LEFT 4
#define SYNCL1_RIGHT 3
#define SYNCL1_UP 2
#define SYNCL1_DOWN 1
#define SYNCL1_FIRE 0

#define SYNCL2_LEFT 0
#define SYNCL2_RIGHT 1
#define SYNCL2_UP 2
#define SYNCL2_DOWN 3
#define SYNCL2_FIRE 4

#endif

#define LED_PORT PORTB
#define LED_DIR DDRB
#define LED_PIN PORTB5

PROGMEM static const char STR_ZX_KEYB_INIT[]="zx_keyb_init...\n";
PROGMEM static const char STR_ZX_KEYB_KEMPSTON[]="zx_keyb: kempston: ";
PROGMEM static const char STR_ZX_KEYB_SINCLAIR1[]="zx_keyb: sinclair1: ";
PROGMEM static const char STR_ZX_KEYB_SINCLAIR2[]="zx_keyb: sinclair2: ";

unsigned char syncl1_bits, syncl2_bits;

void zx_keyb_init()
{
    console_puts_P(STR_ZX_KEYB_INIT);
    syncl1_bits = syncl2_bits = 0xff;

    KEMP_DIR |= KEMP_ALL;
#ifdef CONTROLLER_I2C
    KEMP_PORT |= KEMP_ALL;
#else /*serial controller*/
    KEMP_PORT &= ~KEMP_ALL;
#endif

#ifdef CONTROLLER_I2C
    COL_DIR1 &= ~COL_ALL1;
    COL_PORT1 |= COL_ALL1;

    COL_DIR2 &= ~COL_ALL2;
    COL_PORT2 |= COL_ALL2;

    ROW_DIR &= ~(_BV(ROW3)|_BV(ROW4));
    ROW_PORT |= _BV(ROW3)|_BV(ROW4);

    PCMSK1 |= _BV(COL0INT)|_BV(COL1INT)|_BV(COL2INT)|_BV(COL3INT);
    PCMSK2 |= _BV(COL4INT);
    PCICR |= _BV(PCIE1)|_BV(PCIE2);
#endif
    LED_DIR |= _BV(LED_PIN);
    zx_keyb_led(0);
    sei();
}

void zx_keyb_set_kepston(union zx_joy *joy)
{
    console_put_msg(STR_ZX_KEYB_KEMPSTON, joy->data);

#ifdef CONTROLLER_I2C
    if(joy->bits.left)
        KEMP_PORT &= ~_BV(KEMP_LEFT);
    else
        KEMP_PORT |= _BV(KEMP_LEFT);
    if(joy->bits.right)
        KEMP_PORT &= ~_BV(KEMP_RIGHT);
    else
        KEMP_PORT |= _BV(KEMP_RIGHT);
    if(joy->bits.up)
        KEMP_PORT &= ~_BV(KEMP_UP);
    else
        KEMP_PORT |= _BV(KEMP_UP);
    if(joy->bits.down)
        KEMP_PORT &= ~_BV(KEMP_DOWN);
    else
        KEMP_PORT |= _BV(KEMP_DOWN);
    if(joy->bits.fire)
        KEMP_PORT &= ~_BV(KEMP_FIRE);
    else
        KEMP_PORT |= _BV(KEMP_FIRE);
#else /*serial controller*/
    if(!joy->bits.left)
        KEMP_PORT &= ~_BV(KEMP_LEFT);
    else
        KEMP_PORT |= _BV(KEMP_LEFT);
    if(!joy->bits.right)
        KEMP_PORT &= ~_BV(KEMP_RIGHT);
    else
        KEMP_PORT |= _BV(KEMP_RIGHT);
    if(!joy->bits.up)
        KEMP_PORT &= ~_BV(KEMP_UP);
    else
        KEMP_PORT |= _BV(KEMP_UP);
    if(!joy->bits.down)
        KEMP_PORT &= ~_BV(KEMP_DOWN);
    else
        KEMP_PORT |= _BV(KEMP_DOWN);
    if(!joy->bits.fire)
        KEMP_PORT &= ~_BV(KEMP_FIRE);
    else
        KEMP_PORT |= _BV(KEMP_FIRE);
#endif
}

void zx_keyb_set_sinclair1(union zx_joy *joy)
{
    console_put_msg(STR_ZX_KEYB_SINCLAIR1, joy->data);

    if(joy->bits.left)
        syncl1_bits &= ~_BV(SYNCL1_LEFT);
    else
        syncl1_bits |= _BV(SYNCL1_LEFT);
    if(joy->bits.right)
        syncl1_bits &= ~_BV(SYNCL1_RIGHT);
    else
        syncl1_bits |= _BV(SYNCL1_RIGHT);
    if(joy->bits.up)
        syncl1_bits &= ~_BV(SYNCL1_UP);
    else
        syncl1_bits |= _BV(SYNCL1_UP);
    if(joy->bits.down)
        syncl1_bits &= ~_BV(SYNCL1_DOWN);
    else
        syncl1_bits |= _BV(SYNCL1_DOWN);
    if(joy->bits.fire)
        syncl1_bits &= ~_BV(SYNCL1_FIRE);
    else
        syncl1_bits |= _BV(SYNCL1_FIRE);
}

void zx_keyb_set_sinclair2(union zx_joy *joy)
{
    console_put_msg(STR_ZX_KEYB_SINCLAIR2, joy->data);

    if(joy->bits.left)
        syncl2_bits &= ~_BV(SYNCL2_LEFT);
    else
        syncl2_bits |= _BV(SYNCL2_LEFT);
    if(joy->bits.right)
        syncl2_bits &= ~_BV(SYNCL2_RIGHT);
    else
        syncl2_bits |= _BV(SYNCL2_RIGHT);
    if(joy->bits.up)
        syncl2_bits &= ~_BV(SYNCL2_UP);
    else
        syncl2_bits |= _BV(SYNCL2_UP);
    if(joy->bits.down)
        syncl2_bits &= ~_BV(SYNCL2_DOWN);
    else
        syncl2_bits |= _BV(SYNCL2_DOWN);
    if(joy->bits.fire)
        syncl2_bits &= ~_BV(SYNCL2_FIRE);
    else
        syncl2_bits |= _BV(SYNCL2_FIRE);
}

void zx_keyb_led(char is_on)
{
    if(is_on)
        LED_PORT |= _BV(LED_PIN);
    else
        LED_PORT &= ~_BV(LED_PIN);
}

#ifdef CONTROLLER_I2C
ISR(PCINT1_vect) 
{
    unsigned char enable_rows = 0;

    //console_putc('1');

    if((COL_PIN1 & _BV(COL0)) == 0) {
        if(syncl1_bits & _BV(COL0)) {
            ROW_PORT |= _BV(ROW3);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW3);
        }
        if(syncl2_bits & _BV(COL0)) {
            ROW_PORT |= _BV(ROW4);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW4);
        }
    }

    if((COL_PIN1 & _BV(COL1)) == 0) {
        if(syncl1_bits & _BV(COL1)) {
            ROW_PORT |= _BV(ROW3);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW3);
        }
        if(syncl2_bits & _BV(COL1)) {
            ROW_PORT |= _BV(ROW4);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW4);
        }
    }

    if((COL_PIN1 & _BV(COL2)) == 0) {
        if(syncl1_bits & _BV(COL2)) {
            ROW_PORT |= _BV(ROW3);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW3);
        }
        if(syncl2_bits & _BV(COL2)) {
            ROW_PORT |= _BV(ROW4);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW4);
        }
    }

    if((COL_PIN1 & _BV(COL3)) == 0) {
        if(syncl1_bits & _BV(COL3)) {
            ROW_PORT |= _BV(ROW3);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW3);
        }
        if(syncl2_bits & _BV(COL3)) {
            ROW_PORT |= _BV(ROW4);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW4);
        }
    }

    if(enable_rows) {
        ROW_DIR |= _BV(ROW3)|_BV(ROW4);
    } else {
        ROW_DIR &= ~(_BV(ROW3)|_BV(ROW4));
        ROW_PORT &= ~(_BV(ROW3)|_BV(ROW4));
    }
}

ISR(PCINT2_vect) 
{
    unsigned char enable_rows = 0;

    //console_putc('2');

    if((COL_PIN2 & _BV(COL4)) == 0) {
        if(syncl1_bits & _BV(COL4)) {
            ROW_PORT |= _BV(ROW3);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW3);
        }
        if(syncl2_bits & _BV(COL4)) {
            ROW_PORT |= _BV(ROW4);
        } else {
            enable_rows = 1;
            ROW_PORT &= _BV(ROW4);
        }
    }

    if(enable_rows) {
        ROW_DIR |= _BV(ROW3)|_BV(ROW4);
    } else {
        ROW_DIR &= ~(_BV(ROW3)|_BV(ROW4));
        ROW_PORT &= ~(_BV(ROW3)|_BV(ROW4));
    }
}
#endif
