#include <platform/utils.h>
#include <console/console.h>
#include <timer/timer.h>
#include "reset_controller.h"

/*
struct reset_controller {
    int wdt_reset:1;
    int brown_out_reset:1;
    int extrn_reset:1;
    int pwr_on_res:1;

    struct timer t;
};

struct timer
{
    timer period, ms
    unsigned short period;
    void (*callback)(unsigned char timer_id, const void *data);
    unsigned short last_time;
    const void *data;
};

PORTD7 out       reset with delay

*/

struct reset_controller {
    int wdt_reset:1;
    int brown_out_reset:1;
    int extrn_reset:1;
    int pwr_on_reset:1;

    struct timer t;
};

struct reset_controller rc;

PROGMEM static const char STR_RESET_PWR_ON[]="Power on reset detected\n";
PROGMEM static const char STR_RESET_WDT[]="WatchDog reset detected\n";
PROGMEM static const char STR_RESET_PWR[]="Power failure reset detected\n";
PROGMEM static const char STR_RESET_BTN[]="Button reset detected\n";
PROGMEM static const char STR_RESET_SKIP[]="No reset requested\n";

static void reset_controller_pulse_callback(unsigned char timer_id, const void *data)
{
    timer_free(timer_id);
    PORTD|=_BV(PORTD7);
    DDRD&=~_BV(PORTD7); /*input with pull up*/
}

static void reset_controller_delay_callback(unsigned char timer_id, const void *data)
{
    if(timer_id != 0xff)
        timer_free(timer_id);
    rc.t.period   = 100;
    rc.t.callback = reset_controller_pulse_callback;
    timer_setup(&rc.t);
    PORTD&=~_BV(PORTD7);
    DDRD|=_BV(PORTD7);   /*output low*/
}

void reset_controller_init()
{
    PORTD|=_BV(PORTD7);
    DDRD&=~_BV(PORTD7); /*input with pull up*/
    rc.wdt_reset       = MCUSR&_BV(WDRF)  ? 1 : 0;
    rc.brown_out_reset = MCUSR&_BV(BORF)  ? 1 : 0;
    rc.extrn_reset     = MCUSR&_BV(EXTRF) ? 1 : 0;
    rc.pwr_on_reset    = MCUSR&_BV(PORF)  ? 1 : 0;
    MCUSR=0;
}

void reset_controller_start()
{
    unsigned period = 0;

    if(rc.wdt_reset) {
        console_puts_P(STR_RESET_WDT);
    }
    if(rc.brown_out_reset) {
        console_puts_P(STR_RESET_PWR);
        period=300;
    }
    if(rc.extrn_reset) {
        console_puts_P(STR_RESET_BTN);
        period=10;
    }
    if(rc.pwr_on_reset) {
        console_puts_P(STR_RESET_PWR_ON);
        period=600;
    }

    if(period == 0) {
        console_puts_P(STR_RESET_SKIP);
        return;
    }
    rc.t.period   = period;
    rc.t.callback = reset_controller_delay_callback;
    timer_setup(&rc.t);
}

void reset_controller_do_reset()
{
    reset_controller_delay_callback(0xff,NULL);
}
