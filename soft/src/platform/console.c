#include <console/console.h>
#include "hw_uart.h"

static char uart_no;
void console_platform_init()
{
    hw_uart_register(&uart_no,1);
    register_console(uart_no);
}
