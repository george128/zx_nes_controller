#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <console/console.h>
#include "nes_controller_serial.h"

#define NES_SERIAL_DEBUG


/*
PORTD3 out       nes clk
PORTD4 out       nes latch
PORTD5 inp       nes data1
PORTD6 inp       nes data2
*/

#define CLK PORTD3
#define LATCH PORTD4
#define DATA1 PORTD5
#define DATA2 PORTD6

PROGMEM static const char STR_NES_SERIAL_INIT[]="NES serial: init\n";
PROGMEM static const char STR_NES_SERIAL_READ[]="NES serial: read: ";

void nes_controller_serial_init()
{
    console_puts_P(STR_NES_SERIAL_INIT);
    DDRD |= _BV(CLK);
    DDRD |= _BV(LATCH);
    DDRD &= ~_BV(DATA1);
    DDRD &= ~_BV(DATA2);

    PORTD |= _BV(CLK);
    PORTD &= ~_BV(LATCH);
    PORTD |= _BV(DATA1);
    PORTD |= _BV(DATA2);
}

void nes_controller_serial_read(struct nes_controllers_serial *c)
{
    unsigned char i;

    c->c1.data=c->c2.data=0;

    PORTD |=_BV(LATCH);
    _delay_us(12);
    PORTD &=~_BV(LATCH);

    for(i=0;i<16;i++) {
        PORTD &=~ _BV(CLK);
        _delay_us(6);

        c->c1.data<<=1;
        c->c2.data<<=1;

        if(PIND &_BV(DATA1)) {
            c->c1.data|=1;
        }
        if(PIND &_BV(DATA2)) {
            c->c2.data|=1;
        }
        PORTD |= _BV(CLK);
        _delay_us(6);
    }

    c->p1 = PIND&_BV(DATA1) ? 0 : 1;
    c->p2 = PIND&_BV(DATA2) ? 0 : 1;

#ifdef NES_SERIAL_DEBUG
    if((c->p1 || c->p2) &&
        (c->c1.data != 0xffff || c->c2.data != 0xffff)) {
        console_puts_P(STR_NES_SERIAL_READ);
        console_putd(c->p1);
        console_putd(c->c1.data>>8);
        console_putd(c->c1.data);
        console_putc(' ');
        console_putd(c->p2);
        console_putd(c->c2.data>>8);
        console_putd(c->c2.data);
        console_next_line();
    }
#endif
}
