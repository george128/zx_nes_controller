#ifndef _ZX_KEYB_H_
#define _ZX_KEYB_H_

union zx_joy {
    struct {
        int left:1;
        int right:1;
        int up:1;
        int down:1;
        int fire:1;
    } bits;
    unsigned char data;
};

void zx_keyb_init();
void zx_keyb_set_kepston(union zx_joy *joy);
void zx_keyb_set_sinclair1(union zx_joy *joy);
void zx_keyb_set_sinclair2(union zx_joy *joy);
void zx_keyb_led(char is_on);

#endif /*_ZX_KEYB_H_*/
