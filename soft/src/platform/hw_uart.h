#ifndef _HW_UART_H_
#define _HW_UART_H_

#include <avr/io.h>
#include <avr/pgmspace.h>

#define BAUD_TOL 3 /*to avoid compiler warning*/
#define BAUD 115200
#include <util/setbaud.h>

#include <uart/uart.h>

#if USE_2X 
#define hw_uart_init(num) \
static void hw_uart##num##_init() \
{      \
    UBRR##num = UBRR_VALUE; \
    UCSR##num##A |= _BV(U2X##num); \
    UCSR##num##B = _BV(RXEN##num)|_BV(TXEN##num); \
}
#else
#define hw_uart_init(num) \
static void hw_uart##num##_init() \
{\
    UBRR##num = UBRR_VALUE; \
    UCSR##num##A &= ~_BV(U2X##num); \
    UCSR##num##B = _BV(RXEN##num)|_BV(TXEN##num); \
}
#endif

#define hw_uart_putc(num) \
static void hw_uart##num##_putc(char ch) \
{ \
    while(!(UCSR##num##A & _BV(UDRE##num))); /*wait unitil transmitter be empty*/ \
    UDR##num = ch; \
}

#define hw_uart_input_check(num) \
static char hw_uart##num##_input_check(char timeout_ms) \
{ \
    return (UCSR##num##A & _BV(RXC##num)); \
}

#define hw_uart_readc(num) \
static char hw_uart##num##_readc() \
{\
    while (!hw_uart##num##_input_check(0)); /* Wait for data to be received */ \
    return UDR##num; \
}


#define hw_uart_dev(num, dev_name)                          \
PROGMEM static const char hw_uart##num##_name[]=dev_name; \
PROGMEM static const struct uart_dev hw_uart_dev##num = \
{ \
    .init=hw_uart##num##_init, \
    .putc=hw_uart##num##_putc, \
    .readc=hw_uart##num##_readc, \
    .input_check=hw_uart##num##_input_check, \
    .name=hw_uart##num##_name, \
};

void hw_uart_register(char *uart_no_arr, char max_uarts);

#endif /*_HW_UART_H_*/
