#ifndef _NES_CONTROLLER_SERIAL_H_
#define _NES_CONTROLLER_SERIAL_H_

union nes_controller_serial {
    unsigned short data;
    struct {
        int unused:4;
        int r:1;
        int l:1;
        int x:1;
        int a:1;
        int right:1;
        int left:1;
        int down:1;
        int up:1;
        int start:1;
        int sel:1;
        int y:1;
        int b:1;
    } bits;
};

struct nes_controllers_serial {
    union nes_controller_serial c1,c2;
    char p1,p2;
};

void nes_controller_serial_init();
void nes_controller_serial_read(struct nes_controllers_serial *c);

#endif /*_NES_CONTROLLER_SERIAL_H_*/
