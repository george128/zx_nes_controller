#include <platform/i2c.h>
#include <platform/utils.h>
#include <console/console.h>

#include "tca9548.h"

PROGMEM static const char STR_I2C_CNTR_WRITE_ERR[]="i2c switch(TCA9548) write error: ";

char i2c_switch_set_line(char line_no)
{
    char ret;

    ret=i2c_write(I2C_DEV_NO_TCA9548,1<<line_no,0,NULL);
    if(ret != 0) {
        console_putd10(line_no);
        console_put_msg(STR_I2C_CNTR_WRITE_ERR, ret);
        return 0;
    }
    _delay_us(50);
    return 1;
}
