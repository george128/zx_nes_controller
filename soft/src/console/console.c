#include <platform/utils.h>
#include <console/console.h>
#include <utils/strconv.h>
#include <uart/uart.h>

#define MAX_CONSOLES 2

PROGMEM static const char STR_CONSOLE_INIT1[]="console: registered ";
PROGMEM static const char STR_CONSOLE_INIT2[]=" console(s)\n";

static char consoles[MAX_CONSOLES];
static unsigned char console_idx;

void console_init()
{
    console_idx = 0;
    debug_puts("console_init: started\n");
    console_platform_init();
    console_puts_P(STR_CONSOLE_INIT1);
    console_putd(console_idx);
    console_puts_P(STR_CONSOLE_INIT2);
}

void register_console(char uart_no)
{
    if(console_idx == MAX_CONSOLES) {
        debug_puts("register console: stack overflow!!!\n");
        return;
    }
    
    debug_print("register console no: !!!\n", uart_no);
    consoles[console_idx]=uart_no;
    console_idx++;
}

void console_putc(char ch)
{
    unsigned char i;

    for(i=0;i<console_idx;i++) {
        uart_putc(consoles[i], ch);
    }
}

char console_read(char timeout_ms)
{
    unsigned char i;
    char rb;

    for(i=0;i<console_idx;i++) {
        if((rb=uart_read(consoles[i], timeout_ms))>0) {
            return rb;
        }
    }
    return -1;
}


void console_puts(const char *str)
{
    char ch;
    while((ch=*(str++))!=0) {
        console_putc(ch);
    }
}

void console_puts_P(const char *str)
{
    char ch;
    while((ch=pgm_read_byte(str++))!=0) {
        console_putc(ch);
    }
}

void console_putd(char byte)
{
    char str[3];
    console_puts(byte2str(byte, str));
}

void console_putd10(char d)
{
    char buf[4];
    console_puts(byte10_2_str(d, buf));
}

void console_putmem(const void *ptr, char size)
{
    char i;
    for(i=0;i<size;i++) {
        if(i>0)
            console_putc(' ');
        console_putd(((char *)ptr)[i]);
    }
}

void console_put_short(unsigned short d)
{
    char buf[6];
    console_puts(short10_2_str(d, buf));
}

void console_put_long(unsigned long d)
{
    char buf[20];
    console_puts(long10_2_str(d, buf));
}
