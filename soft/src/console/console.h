#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <uart/uart.h>

void console_init();

void console_putc(char ch);
void console_puts(const char *str);
void console_puts_P(const char *str);
void console_putd(char byte);
void console_putd10(char byte);
void console_put_short(unsigned short d);
void console_put_long(unsigned long d);
void console_putmem(const void *ptr, char size);

char console_read(char timeout_ms);

#define console_next_line() console_putc('\n')
#define console_put_msg(msg, val) {             \
    console_puts_P(msg);\
    console_putd(val);\
    console_next_line();\
    }
#define console_put_msg_short(msg, val) {       \
    console_puts_P(msg);\
    console_put_short(val);\
    console_next_line(); \
    }

void console_platform_init();
void register_console(char uart_no);

#endif /*_CONSOLE_H_*/
